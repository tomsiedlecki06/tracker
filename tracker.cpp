#include <iostream>
#include <string>
#include <fstream>
#include "dish.h"
#include "file_read.h"
#include "file_write.h"
#include "functions.h"
#include "day.h"
#include "date.h"

//OBJECTS
file_read file_out("dishes.txt");
file_write file_in("dishes.txt");



int main()
{

	int choice = 0;
	bool is_running = true;
	while (is_running)
	{
		options_display();
		std::cin >> choice;
		std::cout << std::endl;
		std::string name;
		dish food(0,"",0,0,0,"",0);
		switch (choice)
		{
		case 1:
			//add dish
			file_in.add_line(create_dish());
			break;

		case 2:
			//display dish

			std::cout << "Podaj nazwe dania: ";
			std::cin >> name;
			if (file_out.index_of_dish(name, food))
			{
				food.display();
			}
			else
			{
				std::cout << "Z�a nazwa" << std::endl;
			}
			break;

		case 3:
			//display all dish
			file_out.read_all();

			break;

		case 4:
			//clear db
			file_in.clear();
			break;

		default:
			break;
		}

	}
}


	


