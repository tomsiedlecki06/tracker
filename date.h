#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

class date
{
private:
	int day;
	int month;
	int year;
public:

	date();

	date(int day, int month, int year);

	//GETERY

	int get_day();

	int get_month();
	
	int get_year();
	
	//SETERY

	void set_day(int day);

	void set_month(int month);

	void set_year(int year);
	
	//INNE

	void display();
};

