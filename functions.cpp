#include "functions.h"
bool is_int(std::string x)//sprawdza czy zmienna to int
{
	bool result = false;
	int size = x.size();
	int i;
	for (i = 0; i < size; i++)//petla po caly x
	{
		if (x[i] >= 48 && x[i] <= 57)//jesli x to liczba
		{
			result = true;
		}
		else//jesli x to nie liczba
		{
			result = false;
			break;
		}
	}
	if (size == 0)//jesli dlugosc stringa to 0
	{
		result = false;
	}
	return result;
}

int is_int_display(std::string display)//wyswietla display dopoki wartosc podana przez uzytkownika bedzie int DISPLAY MUSI MIEC ": "
{
	std::string result;
	do
	{
		std::cout << display;
		std::cin >> result;
		std::cout << std::endl;
	} while (!is_int(result));
	return stoi(result);
}

dish create_dish()//tworzy obiekt typu "dish" na podstawie odpowiedzi uzytkownika
{	
	int id;
	file_read read("dishes.txt");
	id = read.count_lines() + 1;//+1 bo to jest liczba lini a id musi byc wieksze o 1

	int if_descryption;

	std::string name;
	std::cout << "Name: ";
	std::cin >> name;
	std::cout << std::endl;
	int protein = is_int_display("Protein(grams): ");
	int fat = is_int_display("Fat(grams): ");
	int carbs = is_int_display("Carbs(grams): ");
	if_descryption = is_int_display("no Desciption(0)/Descryption(any other number)");
	std::string description;
	if (if_descryption)
	{
		std::cout << "Description: ";
		std::cin >> description;
		std::cout << std::endl;
	}
	else
	{
		description = "";
	}
	int weight = is_int_display("wieght(grams): ");
	dish result(id, name, protein, fat, carbs, description, weight);
	return result;
}

dish parse_to_dish(char sign, std::string origin)//dzieli string pobrany z plik na elemety obiektu dish i zwraca nowy obiekt z tymi elementami
{
	dish food(0, "", 0, 0, 0, "", 0);//defaultowe wartosci tutaj a nie w konstrukotrze(cos ne gralo z kosntrutkorem)
	std::string buffor = "";
	int i = 0;
	for (i; i < origin.size(); i++)//mozna ulepszyc id
	{
		if (origin[i] != sign)
		{
			buffor += origin[i];
		}
		else
		{
			food.set_id(stoi(buffor));
			buffor = "";
			i++;
			break;
		}
	}
	for (i; i < origin.size(); i++)//mozna ulepszyc name
	{
		if (origin[i] != sign)
		{
			buffor += origin[i];
		}
		else
		{
			food.set_name(buffor);
			buffor = "";
			i++;
			break;
		}
	}
	for (i; i < origin.size(); i++)//mozna ulepszyc protein
	{
		if (origin[i] != sign)
		{
			buffor += origin[i];
		}
		else
		{
			food.set_protein(stoi(buffor));
			buffor = "";
			i++;
			break;
		}
	}
	for (i; i < origin.size(); i++)//mozna ulepszyc fat
	{
		if (origin[i] != sign)
		{
			buffor += origin[i];
		}
		else
		{
			food.set_fat(stoi(buffor));
			buffor = "";
			i++;
			break;
		}
	}
	for (i; i < origin.size(); i++)//mozna ulepszyc carbs
	{
		if (origin[i] != sign)
		{
			buffor += origin[i];
		}
		else
		{
			food.set_carbs(stoi(buffor));
			buffor = "";
			i++;
			break;
		}
	}
	for (i; i < origin.size(); i++)//mozna ulepszyc description
	{
		buffor += origin[i];
	}
	food.set_description(buffor);
	buffor = "";
	i++;
	return food;
}

void options_display()
{
	std::cout << "OPTIONS:" << std::endl;
	std::cout << std::endl;
	std::cout << "1 => Add dish" << std::endl;
	std::cout << "2 => Display dish" << std::endl;
	std::cout << "3 => Display all dishes" << std::endl;
	std::cout << "4 => Clear database" << std::endl;
	std::cout << std::endl;

}