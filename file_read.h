#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "dish.h"
#include "functions.h"

class file_read
{
private:
	std::fstream document;
	std::string path;
public:

	file_read(std::string path);//sciezka

	int count_lines();

	void read_all();//zwraca tablice obiektow typu dish

	int index_of_dish(std::string name, dish& danie);//zwraca indeks danego dania w bazie danych lub 0 (brak takiego dania) i jezeli jest to modyfikuje referencje
};