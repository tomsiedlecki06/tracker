#include "day.h"

day::day()
{

}

void day::set_date(date data)
{
	this->data = data;
}

date day::get_date()
{
	return data;
}


void day::add_dish(dish food)
{
	dishes.push_back(food);
}

void day::display_dishes()
{
	for (int i = 0; i < dishes.size(); i++)
	{
		dishes[i].display();
	}
}

std::vector<dish> day::get_dishes()
{
	return dishes;
}


