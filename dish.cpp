#include "dish.h"

dish::dish(int id, std::string name, int protein, int fat, int carbs, std::string description,int weight)
{
	this->id = id;
	this->name = name;
	this->protein = protein;
	this->fat = fat;
	this->carbs = carbs;
	this->description = description;
	this->weight = weight;
}
//GETERY
int dish::get_id()
{
	return id;
}
std::string dish::get_name()
{
	return name;
}
int dish::get_protein()
{
	return protein;
}
int dish::get_fat()
{
	return fat;
}
int dish::get_carbs()
{
	return carbs;
}
std::string dish::get_description()
{
	return description;
}
int dish::get_weight()
{
	return weight;
}
//SETERY
void dish::set_name(std::string name)
{
	this->name = name;
}
void dish::set_protein(int protein)
{
	this->protein = protein;
}
void dish::set_fat(int fat)
{
	this->fat = fat;
}
void dish::set_carbs(int carbs)
{
	this->carbs = carbs;
}
void dish::set_id(int id)
{
	this->id = id;
}
void dish::set_description(std::string description)
{
	this->description = description;
}
void dish::set_weight(int weight)
{
	this->weight = weight;
}
//INNE
void dish::display()
{
	std::cout << "id: " << id << std::endl;
	std::cout << "name: " << name << std::endl;
	std::cout << "protein: " << protein << std::endl;
	std::cout << "fat: " << fat << std::endl;
	std::cout << "carbs: " << carbs << std::endl;
	std::cout << "description: " << description << std::endl;
	std::cout << std::endl;
}
