#include "date.h"

date::date()
{

}

date::date(int day, int month, int year)
	: day(day), month(month), year(year) {}

//GETERY

int date::get_day()
{
	return day;
}

int date::get_month()
{
	return month;
}

int date::get_year()
{
	return year;
}

//SETERY

void date::set_day(int day)
{
	this->day = day;
}

void date::set_month(int month)
{
	this->month = month;
}

void date::set_year(int year)
{
	this->year = year;
}

//INNE

void date::display()
{
	std::cout << day << "/" << month << "/" << year << std::endl;
	std::cout << std::endl;
}

