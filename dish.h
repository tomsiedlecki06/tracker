#pragma once
#include <iostream>
#include <string>
#include <fstream>

class dish
{
private:
	int id;
	std::string name;
	int protein;
	int fat;
	int carbs;
	std::string description;
	int weight;
public:
	dish(int id, std::string name, int protein, int fat, int carbs, std::string description, int weight);
	//GETERY
	int get_id();

	std::string get_name();

	int get_protein();

	int get_fat();

	int get_carbs();

	std::string get_description();

	int get_weight();
	//SETERY
	void set_name(std::string name);

	void set_protein(int protein);

	void set_fat(int fat);

	void set_carbs(int carbs);

	void set_id(int id);

	void set_description(std::string description);

	void set_weight(int weight);


	//INNE
	void display();
};
