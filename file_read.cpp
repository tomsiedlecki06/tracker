#include "file_read.h"

file_read::file_read(std::string path)//sciezka
{
	this->path = path;
}
int file_read::count_lines()
{
	int counter = 0;
	document.open(path, std::ios::in);
	if (document.is_open())
	{
		std::string buffor;
		while (getline(document, buffor))
		{
			counter++;
		}
		return counter;
	}
}
void file_read::read_all()//zwraca tablice obiektow typu dish
{
	document.open(path, std::ios::in);
	if (document.is_open())
	{
		std::string buffor;
		while (getline(document, buffor))
		{
			dish food = parse_to_dish('/', buffor);
			food.display();
			std::cout << std::endl;
		}
	}
	document.close();
}

int file_read::index_of_dish(std::string name, dish & danie)//zwraca indeks danego dania w bazie danych lub 0 (brak takiego dania) i jezeli jest to modyfikuje referencje
{
	document.open(path, std::ios::in);
	if (document.is_open())
	{
		bool if_exists = false;
		std::string buffor;
		while (getline(document, buffor))
		{
			dish food = parse_to_dish('/', buffor);
			if (name == food.get_name())//kiedy szukana nazwa jest znaleziona
			{
				if_exists = true;
				//modyfikacja referencji
				danie = food;
				return food.get_id();
			}
		}
		//jezeli nazwa nie wystapila to zwraca 0 czyli brak indexu (bo indeksowanie jest od 1)
		return 0;
		
	}
	document.close();
}