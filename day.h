#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
//---------------------
#include "dish.h"
#include "date.h"
#include "file_read.h"
#include "functions.h"

class day
{
private:
	date data;
	std::vector<dish> dishes;
public:
	day();
	
	//SETERYdata
	void set_date(date data);

	//GETERY

	date get_date();

	std::vector<dish> get_dishes();

	//INNE
	void add_dish(dish food);

	void display_dishes();
};

